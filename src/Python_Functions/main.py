def sum(init=0):
    if init == 0:
        return None
    result = init

    def append(val=None):
        nonlocal result
        if val is not None:
            result += val
            return append
        return result

    return append


assert sum() is None
assert sum(1)() == 1
assert sum(3)(4)(5)() == 12
assert sum(3)(3)(3)(3)(3)() == 15
assert sum(2)(2)(2)(2)(2)() == 10
