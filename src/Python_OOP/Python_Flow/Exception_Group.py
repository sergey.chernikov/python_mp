try:
    raise AttributeError("AN Error")
except AttributeError:
    print("AttributeError")
except KeyError:
    print("KeyError")
except:
    print("Catch any error")
else:
    print("No exception")
finally:
    print("I will be executed anyway")

try:
    raise ExceptionGroup("MultipleErrors", [ValueError("wrong value"), TypeError("wrong type")])
except* ValueError:
    print("ValueError")
except* TypeError:
    print("TypeError")