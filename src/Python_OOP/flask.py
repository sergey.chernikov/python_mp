from flask import Flask

import os, os.path, inspect, logging, shutil, glob, re, numpy

baseFolder = "/srv/sftpgo/data/sftpgo/test-data/PIM_API"

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')

app = Flask(__name__)

postfix = '/SCMDMAPI'


@app.route(f'{postfix}/v1/<order>/<type>')
def run(order, type):
    marketCode = request.args.get('marketCode')
    cursor = request.args.get('cursor')
    cursor = "start" if cursor == '' else cursor
    path = baseFolder + "/" + order + "/" + type + "/" + marketCode + "/" + cursor

    with open(path) as f:
        lines = f.readlines()
        print(lines)
    return ''.join(lines)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)