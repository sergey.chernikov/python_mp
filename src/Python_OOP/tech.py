import sys
from collections import Counter


class OOPClass:
    a = 500
    def __init__(self):
        self.attribute = "attribute"
        self.attribute2 = {}
        a = 5
        print(locals())
    def __call__(self, *args, **kwargs):
        print(f" __call__: Instance of object called with ({args, kwargs})")

    def __getattr__(self, item): # catch AttributeError
        print(f" __getattr__: Somebody called attribute that doesn't exist {item}")

    def __getattribute__(self, item):
        print(f" __getattribute__: Somebody called attribute by .name {item}")
        return object.__getattribute__(self,item)

    def __getitem__(self, item):
        print(f" __getitem__: Somebody called attribute by [name] {item}")

    def __setattr__(self, key, value):
        print(f" __setattr__: Somebody tries to set attribute {key} with value {value}")
        self.__dict__[key] = value


print(dir(type))



print("Create class instance with assining to test")

# print(type(OOPClass))
test = OOPClass()
test2 = OOPClass()
# test.attribute2['hello'] = 'word'
# print('========================================')
# test.a = 600
# test.b = 700
# OOPClass.a = 900
# print(test.a)
# del OOPClass.a
# print(' del========================================')
# print(test.a)
# print(' del========================================')
# print(test.b)
# print(test2.a)


# test2.attribute1 = 10

test2.__dict__['attribute1'] = 10


print(test2.__dict__)
# print('========================================')
# assert hasattr(test, 'attribute2')
# assert 'attribute2' in test.__dict__
# print(OOPClass.__dict__)
# print(test.__doc__)


sys.exit(0)
print("test(par1, par2)")
test("par1", "par2")
print("test.attribute")
test.attribute
print("test['attribute']")
test['attribute']
print("test.attribute = 12")
test.attribute = 12
print("test.no_attribute")
test.no_attribute
print("hasattr(test, 'no_attribute')")
hasattr(test, 'no_attribute')

sys.exit(0)


class Demo:
    # __slots__ = ('x','y', '__dict__')
    def __init__(self, val):
        self.x = val
        self.y = val
    def some_method(self):
        print('MRO')

demo1 = Demo(1)
demo2 = Demo(2)
demo1.z = 5
print(demo1.__dict__)
print(Demo.__dict__)
print(type(Demo))
print(type(Demo).__dict__)

print(demo1.x)
print(demo2.x)
print(Demo.some_method.__class__)