## Calculating a value that could take a lot of time
def calculate_value(arr):
    for val in arr:
        yield val ** 330


arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]

val_gen = calculate_value(arr)

print(next(val_gen))
print(next(val_gen))
print(next(val_gen))
print(next(val_gen))
print(next(val_gen))


## Generating a value forever
def sequence_next(calculate_fun):
    n = 0
    while True:
        yield n
        n = calculate_fun(n)

def calculate_fun(n):
    return n + 1

sequence_gen = sequence_next(calculate_fun)

print(sequence_gen.__next__())
print(sequence_gen.__next__())
print(sequence_gen.__next__())
print(sequence_gen.__next__())
print(sequence_gen.__next__())

print("==========================================================================")

def next_variant():
    next_variant = None
    val = 0
    while True:
        if next_variant is not None:
            val = val ** 2
        next_variant  = yield val
        val += 1

gen = next_variant()
print(gen.__next__())
print(gen.__next__())
print(gen.send("a value"))
print(gen.send("a value"))


def square_gen(stop_num=10):
    for i in range(0,stop_num):
        yield i ** 2

gen = square_gen(50)
print(list(gen))


print(list(i ** 2 for i in range(0,50) if i % 49 == 0))

#most words
print("============================= Most words =====================================")

def gen_most(sentences):
    prev_word = ""
    for sentence in sentences:

        yield sorted(sentence.split(" "), key=lambda x : len(x))[-1]

gen = gen_most(["I am beautifyl","You are not", "But you have aaaaaaaaaaaaaaaaaaaa cosmetics", "And you have jivot"])

print(list(gen))

# Counting characters
print("========================================= Counting Characters ==============================================")
from data import counting


char_length = (len("".join(sentence.split(" "))) for sentence in counting)

char_length_more_130 = (length for length in char_length if length >= 130)
print(list(char_length_more_130))


print("========================================= Weather Filter ===================================================")
from data import weather_data
import datetime


gen = (data_obj for data_obj in weather_data if data_obj['temp'].isdigit()
       and datetime.strptime("[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}", data_obj['date']) is not None)

print(list(gen))