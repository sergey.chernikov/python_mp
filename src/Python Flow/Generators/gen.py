def gen(arr):
    for el in arr:
        try:
            x = yield el  # send()
            if x is not None:
                print("Gen: ", x)
        except Exception as e:
            print("OMG, I have an Exception: ", e)

print(type(gen([])))
# alternative


print("Standard generator")
arr = [1, 2, 3, 4, 5, 6, 7, 8]

genobj = gen(arr)

print(next(genobj))
# genobj.close() - close - stop generator, in close is called next method will generate StopIteration
print(next(genobj))  # if close that raise StopIteration
print(next(genobj))
print(genobj.send("Generator received a value"))  # send(None) == next(gen)
print(next(genobj))
genobj.throw(Exception, "An exception sent by throw")
print(next(genobj))

print("Alternative generator")
# alternative
genobj_alternative = (el for el in arr)

# print(sum(genobj_alternative))

print(next(genobj_alternative))  # 1
print(next(genobj_alternative))  # 2
print(next(genobj_alternative))  # 3
print(next(genobj_alternative))  # 4
print(next(genobj_alternative))  # 5


# generate file

# with open("big.file", "w+") as file:
#     for i in range(1000000000000000000000):
#         file.write(f"{i}000000000000000000000000000000000000000000000000\n")

# file_obj = open("big.file", "r")

def read_file_with_yield(file):
    n = 0
    for line in file:
        yield f"Number: {n}, line: {line}"
        n += 1


with open("big.file", "r") as file:
    gen = read_file_with_yield(file)
    print(gen.__next__())
    print(gen.__next__())
    print(gen.__next__())
    print(gen.__next__())


# Generator pipeline

def number_gen(start=0, stop=100):
    for i in range(start, stop + 1):
        yield i


def double_number_gen():
    yield from number_gen(0, 10)


double_gen = double_number_gen()

for i in double_gen:
    print(i)
