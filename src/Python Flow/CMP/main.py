name = "test"
length = 9

print("========================= Context Manger Class ============================")
from context import ResourceManager

with ResourceManager(name) as resource:
    resource.consoleprint(length)
print("I am closed" if not resource.is_opened() else "I am still opened and eating memory")

print("====================== Context Manager Function ==============================")

from context import resource_manager

with resource_manager(name) as resource:
    resource.consoleprint(length)
print("I am closed"  if not resource.is_opened() else "I am still opened and eating memory")
