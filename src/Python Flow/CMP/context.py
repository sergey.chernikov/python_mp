from resources import Resource


class ResourceManager:
    def __init__(self, name):  # create context manager
        self.__resource = Resource(name)
        print("Context Manger Class: initialization")

    def __enter__(self):  # enter context manager
        print("Context Manger Class: __enter__")
        self.__resource.open()
        return self.__resource  # return object to work with

    def __exit__(self, exc_type, exc_val, exc_tb):  # exit context manager
        print("Context Manger Class: __exit__")
        if self.__resource.is_opened():
            self.__resource.close()
        if exc_type is not None:
            print(f"Context Manger Class: Exception exc_type {exc_type}, exc_val {exc_val}, exc_tb {exc_tb}")
            print("Context Manger Class: returns True to not raise an exception")
            return True  # return False in the case of raising exception


# ============================== Context Manager =============================
from contextlib import contextmanager


@contextmanager
def resource_manager(name):
    print("Context Manager Function: initialization")
    resource = None
    try:
        resource = Resource(name)
        resource.open()
        print("Context Manager Function: enter")
        yield resource
    except Exception as e:
        print(f"Context Manager Function: Exception {e}")
    finally:
        print("Context Manager Function: exit")
        resource.close()
