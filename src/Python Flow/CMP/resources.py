class Resource:
    def __init__(self, name):
        self.__name = name
        self.__opened = None

    def consoleprint(self, length):
        if length < 10:
            print("Resource name: ",self.__name)
        else:
            raise Exception("I just don't want to print")

    def open(self):
        self.__opened = True

    def close(self):
        self.__opened = False

    def is_opened(self):
        return self.__opened