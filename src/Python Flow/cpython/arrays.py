import time, unittest
from math import floor


def print_execution_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        print(f'Function {func.__name__}  Took {total_time:.4f} seconds')
        return result

    return wrapper


class ArrayTimeComplexity:

    def __init__(self, int_range= 1000):
        self.arr = []
        self.int_range = int_range

    @print_execution_time
    def __init(self) -> None:
        for i in range(1, self.int_range):
            self.arr.append(i)

    @print_execution_time
    def __array_beginning_element_add(self) -> None:
        self.arr.insert(0, 100)
        assert True

    @print_execution_time
    def __array_middle_element_add(self) -> None:
        self.arr.insert(floor(self.int_range / 2), 100)

    @print_execution_time
    def __array_end_element_add(self) -> None:
        self.arr.append(100)

    @print_execution_time
    def __array_beginning_element_del(self) -> None:
        self.arr.pop(0)
        assert True

    @print_execution_time
    def __array_middle_element_del(self) -> None:
        self.arr.pop(floor(self.int_range / 2))

    @print_execution_time
    def __array_end_element_del(self) -> None:
        self.arr.pop(self.int_range)

    def run(self):
        self.__init()
        self.__array_beginning_element_add()
        self.__array_middle_element_add()
        for i in range(10):
            self.__array_end_element_add()
        self.__array_end_element_del()
        self.__array_middle_element_del()
        self.__array_beginning_element_del()


arrays = ArrayTimeComplexity(100000000)
arrays.run()
