
import dis
import ast
def sum(a=0,b=0):
    return a + b

print('*************** sum bytecode *****************')
dis.dis(sum)

[sum(i,i) for i in range(10)]

print('*************** sum bytecode int optimisation*****************')
dis.dis(sum, show_caches=True, adaptive=True)

[sum("a","b") for i in range(1000)]

print('*************** sum bytecode str optimisation*****************')
dis.dis(sum, show_caches=True, adaptive=True)



print('*************** run bytecode file *********************')
with open('byte_code_test.pyc', 'rb') as file:
    byte_code = file.read()
    exec(byte_code)

# ast_obj = ast.parse('(a+b)')

# print(ast_obj)
# print(ast.unparse(ast_obj))



# print('*************** code object *****************')
# code_object = compile(source="print('Hello from code object')", filename="hello_test.py", mode='exec')
# dis.dis(code_object)