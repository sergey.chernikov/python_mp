"""
Bytes vs string task 2: Rewrite previous program to work with 200 GB binary files.
"""
import sys
from collections import Counter

import time

def count_execution_time(fun):
    def wrapper(full_file_path, most_common=None):
        start = time.time()
        fun(full_file_path, most_common)
        end = time.time()
        print(f"\nExecution time: {end - start}")
    return wrapper

def print_out_symbol(most_common_symbols, encoding):
    print('| {:^10} | {:^10} |'.format("Symbol", "Count"))
    print('===========================')
    for symbol, count in most_common_symbols:
        symbol = str(symbol if symbol.isprintable() else symbol.encode(encoding))
        print('| {:^10} | {:^10} |'.format(symbol, count))


def code_finder(b_array, encoding):
    decoded_content = ""
    buffer = []
    for byte in b_array:
        if len(buffer) > 3:
            buffer.clear()
        buffer.append(byte)
        try:
            decoded_content += bytes(buffer).decode(encoding=encoding)
        except UnicodeDecodeError:
            continue
    return decoded_content

@count_execution_time
def count_most_common_bfile_by_chunk(full_file_path, most_common=10):
    print("Decoding...")
    encoding = "utf-8"
    with open(full_file_path, mode="br") as bfile:
        chunk_size = 4086
        content = bfile.read(chunk_size)
        counter = Counter()
        decoded_content = ""
        while content:
            try:
                decoded_content = content.decode(encoding=encoding)
            except UnicodeDecodeError:
                decoded_content = code_finder(content,encoding)
            finally:
                counter.update(decoded_content)
                content = bfile.read(chunk_size)
        print_out_symbol(counter.most_common(most_common), encoding)
    print("Finished...")


count_most_common_bfile_by_chunk(sys.argv[1],5)
