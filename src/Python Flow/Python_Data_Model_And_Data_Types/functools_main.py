from functools import partial, cache, wraps, cached_property, s

def func(a,b):
    return a + b

two_add_two = partial(func, 2,2)

print(two_add_two())