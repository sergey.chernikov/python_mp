from collections import namedtuple, defaultdict
from functools_main import reduce

arr = [1,2,4,5,6,7,8,9]

print(sorted(arr, reverse=True))


a = [1, 2, 3, 5]
b = ['one', 'two', 'three']
zipped = list(zip(a, b))  # [(1, 'one'), (2, 'two'), (3, 'three')]
print(zipped)

filtered = list(filter(lambda x: x % 2 == 0,arr))
print(filtered)


elem = namedtuple('Student', 'name, age')

el = elem('Alex', '23')
print(el.age)


def foo(**arg):
    for key, val in arg.items():
        print(key, val)

foo(c=23, a=12)


array = ["a1", "d3", "c4"]

print(list(filter(lambda x: x != "c4",array)))


array = ["a", "b", "c"]

print(reduce(lambda x,y:x+y, array))


def func():
    return "privet"

dic = defaultdict(func)

print(dic['2'])

print(dic)


def functionsss(**args):
    print(args)



functionsss(a={"1":"10"}, b={12:12})

