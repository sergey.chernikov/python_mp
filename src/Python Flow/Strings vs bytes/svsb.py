import locale
import encodings as econ

print(f"Current system encoding is {locale.getpreferredencoding(False)}")

symbol = 'A'
print(f"ASCII code for {symbol} is {ord(symbol)}, in HEX is {hex(ord(symbol))}")


encoding = "ASCII"
print(f" Symbol: {symbol.encode(encoding).decode(encoding)} in {encoding} is {symbol.encode(encoding)}")

encoding = "UTF-8"
print(f" Symbol: {symbol} in {encoding} is {symbol.encode(encoding)}")
encoding = "UTF-16"
print(f" Symbol: {symbol} in {encoding} is {symbol.encode(encoding)}")

encoding = "UTF-32"
print(f" Symbol: {symbol} in {encoding} is {symbol.encode(encoding)}")

encoding = "Latin-1"
print(f" Symbol: {symbol} in {encoding} is {symbol.encode(encoding)}")



# help(econ)


#
# symbol = 197
# print(f"Symbol for code {symbol} is {chr(symbol)}")


print("Formatting string with different formats")

print("With % format for 'My name is %s'")

string = ("My name is %s" % 'Ivan')
print(string)

print("With format for 'My name is {}'")
string = "My name is {}".format('Ivan')
print(string)



print("With format for 'My name is {0} {1}'")
string = "My name is {0} {1}".format('Ivan', 'Ivanov')
print(string)

print("With f{} for 'My name is {}'")
string = f"My name is {'Ivan'}"
print(string)

print("sert".find("e"))