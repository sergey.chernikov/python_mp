import functools
import inspect
import time
from collections.abc import Callable


def execution_time(fun: Callable) -> Callable:
    @functools.wraps(fun)  # copy lost metadata
    def wrap(*args, **kwargs):
      """
         you see that message due to not having wraps @functools.wraps annotation
      """
      start_time = time.time()
      result = fun(args, kwargs)
      print("Execution time: ", start_time - time.time())
      return result
    return wrap

def execution_time_ns(message): # decorator params
    def decor(fun):
        @functools.wraps(fun)  # copy lost metadata
        def wrap(*args, **kwargs):
          """
            you see that message due to not having wraps @functools.wraps annotation
          """
          start_time = time.time_ns()
          print(message)
          result = fun(args, kwargs)
          print("Execution time ns: ", start_time - time.time_ns())
          return result
        return wrap
    return decor

class DecoratorClass:

    def __init__(self, message=None):
        self.message = message
        print('__init__')

    def __call__(self, fun):
        def wrap(*args, **kwargs):
            print(self.message)
            print('__call__')
            return fun()
        return wrap


@execution_time_ns(message='test_dec_msg') # decorator params
@execution_time # ↑ - decorator execution order
@DecoratorClass("test_dec_class_msg")
def execute_a_code(*args, **kwargs):
    """
    execute_a_code doc
    :param args:
    :param kwargs:
    :return:
    """
    time.sleep(1)
    return 1

#execute_a_code = execution_time(execute_a_code) # decorator without @annotation

print(execute_a_code.__doc__)
print(execute_a_code())
