
swimmers = {'Mon': 14, 'Tue': 12, 'Wed': 14, 'Fri': 11}

swimmers_tbl = f"""
indices
|----|       |-----hash code (ex)----|-----pointer to the key--------|-----pointer to the val------|
| -1 |       
| -1 |       
|  0 |  ->   | 5207248090200460761   |        ->    Mon              |        ->    14             |
|  1 |  ->   | 5207248090200460762   |        ->    Tue              |        ->    12             |
| -1 |       
|  2 |  ->   | 5207248090200460763   |        ->    Wed              |        ->    14             |
|  3 |  ->   | 5207248090200460764   |        ->    Fri              |        ->    11             |
|  -2|  ->   | 5207248090200460764   |        ->    Any              |        ->    44             | - deleted
|----|       |-----------------------|-------------------------------|-----------------------------|

8-bit                                              192-bit
"""
print(swimmers_tbl)

doc = """
Schema to create dic
1) Setup indices (array of bytes), default 8 buckets, each -1 (empty), 5 would be hold and 1/3 would be -1
2) Add:
    a) hash(el) % len(indices) -> index
    b) indices[index] == -1
        1. yes - add
        2. no - collision
            a. hash code is different?
                yes - probe the next index if -1 use it
3) is indices empy for 1/3 (or ⅔ full)
    yes- do nothing
    no - double and recalculate indices 
!!! note - \033[1m Key-sharing dictionary \033[0m - obj.__dict__ store keys the same place for all instances, but refs in different tables

"""
print(doc)

