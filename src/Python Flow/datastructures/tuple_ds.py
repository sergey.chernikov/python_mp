my_tuple = (1, 2, 3, 6, 5, 6)

print("Tuple:", my_tuple)

print("Slice index[0]", my_tuple[0])

try:
    my_tuple[0] = 2
except Exception as e:
    print(f"You have tried to set my [0] element after creating, but I am immutable: {e}")

print("Index of 6:", my_tuple.index(6))

print("Count of 6:",my_tuple.count(6))