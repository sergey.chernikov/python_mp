# from Scripts.bottle import unicode
from _decimal import Decimal

arr_list = [1,2,3,4,5,6,7,8,9,0]
# slices [start_el:end_el:range, default 1]
print(arr_list[0:5])
print(arr_list[0:9])

print(arr_list[0:10:2]) # each second element



arr_list = [1,2,3,4,5,6,7,8,9,0]

arr_list_copy = arr_list[:] # copy list

print(id(arr_list) !=  id(arr_list_copy))



arr_list = [1,2,3,4,5,6,7,8,9,0]

arr_list_copy = arr_list

arr_list_copy[:] = [1,3,5] #replace elements in the list without creating new object

print(id(arr_list) ==  id(arr_list_copy))

arr_list_copy = [1,3,5]

print(id(arr_list) !=  id(arr_list_copy))

print(arr_list_copy[::2])


set_obj = {"Art", "Bart", "Heart", "Art"}
set_obj_fr = frozenset({"Art", "Bart", "Heart", "Art"})
# print(hash(set_obj))
print(hash(set_obj_fr))



dic = {
    frozenset({"alex", "tamara"}):"11111111111111111",
    "2":"2",
    "3":"4",
    "4":"4",
    "5":"5",
}

print(dic.get("1321", "Not found"))
print(dic)
dic.setdefault("1321", "Not found1")
print(dic)
# for key, value in dic.items():
#     print(key, value)



print(Decimal("0.1") + Decimal("0.7"))

print("\a") #bip

string = "'Gürzenichstraße'"
string = string.encode("UTF-8")

print(string)
for idx, elem in enumerate(string):
    print(idx, elem)

print(b'\xe6\x98\xbe' == b'\xe6\x98\xbe')
print(b'\x00\xe6\x98\xbe'.decode("UTF-8"))
print('\u047e')
print('\N{LATIN CAPITAL LETTER E}\N{COMBINING CIRCUMFLEX ACCENT}')

print(r'A\'B')

print(bin(12))
print(oct(12))
print(hex(12))


class Unhashable:
    def __eq__(self, other):
        return (self == other)

class HashableAgain:
    def __eq__(self, other):
        return (self == other)

    def __hash__(self):
        return id(self)

unhahsble = Unhashable()

hashable = HashableAgain()

hash(hashable)
