# list_coll = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# tuple_coll = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
# set_coll = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
# dic_coll = {
#     0: 0,
#     1: 1,
#     2: 2,
#     3: 3,
#     4: 4,
#     5: 5,
#     6: 6,
#     7: 7,
#     8: 8,
#     9: 9
# }
#
# bytearray_coll = bytearray(3)
#
# print('slicing')
# print(list_coll[0:3])
# print(list_coll[0:9:2])
#
# print(tuple_coll[0:3])
# print(tuple_coll[0:9:2])
#
# print(bytearray_coll[::-1])
# print(bytearray_coll)
#
# print("mutiable")
#
# list_coll.append(10)
# print(list_coll)
#
# set_coll.add(10)
# print(set_coll)
#
# dic_coll[10] = 10
# print(dic_coll)


tuple_col = (1,2,3,4,[1,2,3,4], {1,2,3,4})
print(tuple_col)

tuple_col[5].add(344)

print(tuple_col)

