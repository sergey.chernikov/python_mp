from dataclasses import dataclass


def sum(a=0, b=0):
    return a + b


@dataclass
class CallableObject:
    name: str
    age: int

    def __call__(self, *args, **kwargs):
        print(f"Name: {self.name}, age: {self.age}, args: {args}, kwargs: {kwargs}")


CallableObject('Ignat', 122)()  # as an option as IIF

callable_object = CallableObject('Ignat', 12)

callable_object()

callable_object('test', 'name', test='123')

assert True == callable(callable_object)
assert True == callable(sum)


class Decorator:
    def __init__(self, fun):
        self._fn = fun

    def __call__(self, x, y, *args, **kwargs):
        print("Attention!!! Decorator works as callable")
        return self._fn(x, y)


sum = Decorator(sum)

print(sum(3, 5))


@Decorator
def diff(x, y):
    return y - x


print(diff(7, 8))

print(["34234"])
