class HashableCLSameHash:

    def __init__(self, name):
        self.name = name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        """
            good hash function
                1) different objects are the different hash
                2) the same object is the same hash
                3) always the same hash length
                4) hash cannot be converted back

        """
        return 1  # force make hash the same to have a collision in dict


a = HashableCLSameHash("a")
a_1 = HashableCLSameHash("a")
b = HashableCLSameHash("b")

my_dic = {a: 1, a_1: 11, b: 2}

print("hash: ", hash(a) == hash(a_1) == hash(b))
print("name: ", a.name == a_1.name)
print("eq name:", my_dic.values())

a = HashableCLSameHash("a")
a_1 = HashableCLSameHash("a_1")
b = HashableCLSameHash("b")

my_dic = {a: 1, a_1: 11, b: 2}

print("hash: ", hash(a) == hash(a_1) == hash(b))
print("name: ", a.name == a_1.name)
print("diff name:", my_dic.values())



