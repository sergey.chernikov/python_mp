my_set = {'Mon', 'Tue', 'Wed', 'Thu', 'Fri'} # standard 8 rows (as minimum) hash table
# each element in set must be hashable (__hash__ and __eq__)

my_set_tbl = f""" !!!random solt!!!
|-----hash code---------|----pointer to the key---------|
|         -1            |             empty             |
|         -1            |             empty             |
| 5207248090200460761   |              Mon              |
| 5207248090200460762   |              Tue              |
|         -1            |             empty             |
| 5207248090200460763   |              Wed              |
| 5207248090200460764   |              Fri              |
|-----------------------|-------------------------------|

"""

print(my_set_tbl)

doc = """
to insert:
    1) Calculates hash code
    2) Calculates index hash_code % table_size              ex.: (hash('Mon') % 8) >- 7873938940781819425 % 8 = 1
    3) Empty bucket?                                        ex.: put val on index 1
        yes -> store hash code and pointer to the element
        no  -> :
                matches hash code:
                    no: -> calculate next index (Hash collisions) Incrementing the index after a collision is called linear probing.
                    yes:
                        elem values match: (Siphash algorithm)
                            yes: the same element (do nothing)
                            no:  calculate next index (collision)  
    4)table size filled >  ⅔ ( ⅓ > empty)
            yes: - no resize
            no: -> resize 2x (8 -> 16) and recalculate all hash
            
to search:
    1) Calculate hash
    2) hash_code % table_size = index
    3) get by index:
        no:  no value/false
        yes: 
            a) compare hash
            b) compare eq
            c) return el/true
        
"""
print(doc)

print("performance test")

import time
import random
import string

perf_set = {'1', '2'}

def generate_random_string(length):
    characters = string.ascii_uppercase + string.digits  # Include uppercase letters and digits
    result = ''.join(random.choices(characters, k=length))
    return result
pv_time = 0
for i in range(0, 100000):
    key = generate_random_string(5)
    start_time = time.time_ns()
    perf_set.add(key)
    end_time = time.time_ns()
    time_diff = end_time - start_time
    if pv_time < time_diff:
        print("Add (prev): ", pv_time)
        print("Collision: ", time_diff)
    pv_time = time_diff
