import gc, sys, time

print(" ================================== Turn off CG =========================================")
gc.disable()
print(gc.get_threshold())
print("----------------- Run GC before the program ---------------------")
gc.collect()
print(" ========================================================================================")


class C: pass


print("Create instance of class C with name c and circular refference c.x = c")
c = C()
c.x = c

print("========================= Run GC =========================")
print("Objects in memory: ", gc.get_count())
objects = gc.collect()
print("Objects are collected: ", objects, ", Object C still in memory" if objects == 0 else ", Object C is removed from memory" )
print("Delete external link for circular refference 'del c'")
print(hex(id(c)))
del c
print("========================= Run GC =========================")
objects = gc.collect()
print("Objects are collected: ", objects, ", Object C still in memory" if objects == 0 else ", Object C is removed from memory" )
print("Objects in memory: ", gc.get_count())
