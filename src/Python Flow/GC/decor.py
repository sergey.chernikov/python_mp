from functools import wraps

def fun_decor(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        print('input: ', args)
        res = fun(args)
        print('res: ', res)
        return res
    return wrapper

@fun_decor
def sum(*args):
    return sum(args)

print(sum(*[1,2,3,4,5]))