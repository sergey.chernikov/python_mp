import gc

gc.disable()
def find_object_in_gen(val, gen):
    if not gc.is_tracked(val):
        return False
    for obj in gc.get_objects(generation=gen):
        try:
            if obj == val:
                return True
        except ReferenceError as e:
            continue
    return False

s = "324234$#$%#$@%#@$%@$##$@#$@"

print(find_object_in_gen(s,0))
print(find_object_in_gen(s,1))
print(find_object_in_gen(s,2))