# a = []
# count = 0
#
# while True:
#     count += 1
#     a.append(count)

# import ctypes
import ctypes

# variable declaration
val = 20

# display variable
print("Actual value -", val)

# get the memory address of the python object
# for variable
x = 1887706768112

# display memory address
print("Memory address - ", x)

# get the value through memory address
a = ctypes.cast(1887706768112, ctypes.py_object).value

# display
print("Value - ", a)