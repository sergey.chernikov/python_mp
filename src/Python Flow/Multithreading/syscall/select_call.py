import socket
import select

# Конфигурация сервера
HOST = '127.0.0.1'
PORT = 65432

# Создание сокета
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((HOST, PORT))
server_socket.listen()

# Списки для select
inputs = [server_socket]
outputs = []
message_queues = {}

while inputs:
    print("Ожидание следующего события")
    readable, writable, exceptional = select.select(inputs, outputs, inputs)

    # Обработка входящих данных
    for s in readable:
        if s is server_socket:
            # Новое соединение
            connection, client_address = s.accept()
            print(f"Новое соединение от {client_address}")
            connection.setblocking(0)
            inputs.append(connection)
            message_queues[connection] = []
        else:
            # Получение данных от клиента
            data = s.recv(1024)
            if data:
                print(f'Получены данные "{data.decode()}" от {s.getpeername()}')
                message_queues[s].append(data)
                if s not in outputs:
                    outputs.append(s)
            else:
                # Нет данных, клиент отключился
                print(f"Закрытие соединения с {client_address}")
                if s in outputs:
                    outputs.remove(s)
                inputs.remove(s)
                s.close()
                del message_queues[s]

    # Обработка исходящих данных
    for s in writable:
        try:
            next_msg = message_queues[s].pop(0)
        except IndexError:
            print(f"Очередь вывода для {s.getpeername()} пуста")
            outputs.remove(s)
        else:
            print(f"Отправка данных клиенту {s.getpeername()}")
            s.send(next_msg)

    # Обработка исключительных ситуаций
    for s in exceptional:
        print(f"Обработка исключительной ситуации для {s.getpeername()}")
        inputs.remove(s)
        if s in outputs:
            outputs.remove(s)
        s.close()
        del message_queues[s]

server_socket.close()