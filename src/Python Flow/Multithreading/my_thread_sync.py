import threading

condition = threading.Condition()

def print_numbers(start, max_num=100):
    for number in range(start, max_num + 1, 2):
        with condition:
            print(f"{threading.current_thread().name}: {number}")
            condition.notify()
            if number < max_num:
                condition.wait()


thr_print_even = threading.Thread(target=print_numbers, args=(0,), name="Even")
thr_print_odds = threading.Thread(target=print_numbers, args=(1,), name="Odd")

thr_print_even.start()
thr_print_odds.start()

thr_print_even.join()
thr_print_odds.join()