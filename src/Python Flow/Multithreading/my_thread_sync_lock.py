import threading


def print_numbers(sync_lock, next_lock, start, max_num=100):
    for number in range(start, max_num + 1, 2):
        sync_lock.acquire()
        print(f"{threading.current_thread().name}: {number}")
        next_lock.release()


even_lock = threading.Lock()
odd_lock = threading.Lock()

thr_print_even = threading.Thread(target=print_numbers, args=(even_lock, odd_lock, 0,), name="Even")
thr_print_odds = threading.Thread(target=print_numbers, args=(odd_lock, even_lock, 1,), name="Odd")

odd_lock.acquire()

thr_print_even.start()
thr_print_odds.start()

thr_print_even.join()
thr_print_odds.join()