import threading, time


def my_thread(wait=0):
    while True:
        time.sleep(wait)
        print(f'ID: {threading.current_thread().name} {threading.active_count()}')


rt1 = threading.Thread(target=my_thread, args=(1,), name='Thread 1')
rt5 = threading.Thread(target=my_thread, args=(5,), name='Thread 5')


rt1.start()
rt5.start()


rt1.join()
rt5.join()