import multiprocessing

even_lock = multiprocessing.Lock()
odd_lock = multiprocessing.Lock()


def print_numbers(start, sync_lock, next_lock, max_num=100):
    for number in range(start, max_num + 1, 2):
        sync_lock.acquire()
        print(multiprocessing.current_process().name, number)
        next_lock.release()


if __name__ == '__main__':
    pr_print_even = multiprocessing.Process(target=print_numbers, args=(0, even_lock, odd_lock), name="Even")
    pr_print_odds = multiprocessing.Process(target=print_numbers, args=(1, odd_lock, even_lock), name="Odd")

    odd_lock.acquire()

    pr_print_even.start()
    pr_print_odds.start()

    pr_print_even.join()
    pr_print_odds.join()