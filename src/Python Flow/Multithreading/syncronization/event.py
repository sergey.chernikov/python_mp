import threading, time

event = threading.Event()
max_threads = 3
def producer_condition():
    print("====================================================================================")
    print("Sending Message")
    time.sleep(3)
    print("Message is sent")
    print("====================================================================================")
    time.sleep(3)
    event.set()


def consumer_condition():
    print(f"Consumer {threading.current_thread().name} is created")
    event.wait()
    time.sleep(1)
    print(f"Received message with thread - {threading.current_thread().name}")


consumers = []

for i in range(0, max_threads + 1):
    consumer = threading.Thread(target=consumer_condition, name=f"cons -  {i}")
    consumers.append(consumer)
    consumer.start()

producer = threading.Thread(target=producer_condition)
producer.start()

for consumer in consumers:
    consumer.join()

producer.join()