import threading, time, random

max_wait = 3
max_threads = 6

def barrier_action():
    print(f"=========================={max_wait} are reached barrier ===========================")

barrier = threading.Barrier(max_wait, action=barrier_action)


def process_thread():
    global max_wait
    time.sleep(random.randint(2, 5))
    print(f"Thread {threading.current_thread().name} has reached the barrier")
    barrier.wait()
    print(f"Thread {threading.current_thread().name} passed the barrier {max_wait}")


threads = []

for i in range(0, max_threads):
    thread = threading.Thread(target=process_thread, name=f"thr {i}")
    threads.append(thread)
    thread.start()

for thread in threads:
    thread.join()

print("All threads successfully passed the barrier.")