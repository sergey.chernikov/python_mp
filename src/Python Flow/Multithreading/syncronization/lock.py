import threading
import time, random

lock = threading.Lock()

counter = 0
max_threads = 5
thread_pool = []

def increment():
    global counter
    while counter < 100:
        # lock.acquire()
        with lock:
            print("-------------------------------- lock.acquire() --------------------------------")
            print(f"Thread '{threading.current_thread().name}' is about to increase counter {counter}")
            counter += 1
            print(f"Thread '{threading.current_thread().name}' increased counter {counter}")
            print("-------------------------------- lock.release() --------------------------------")
        # lock.release()



for i in range(0, max_threads):
    thread_pool.append(threading.Thread(target=increment, name=f"I am thread - {i}"))

for thread in thread_pool:
    thread.start()
    # thread.join()


