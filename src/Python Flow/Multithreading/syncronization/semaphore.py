import threading, time

max_connections = 5
semaphore = threading.Semaphore(max_connections)
# semaphore = threading.BoundedSemaphore(max_connections) #it will raise Exception (ValueError: Semaphore released too many times) if count(release()) != count(acquire())


def connect_database():
    with semaphore:
        print(f"Database connect with thread {threading.current_thread().name}")
        print(f"Database operate with thread {threading.current_thread().name}")
        time.sleep(3)
        print(f"Database disconnect with thread {threading.current_thread().name}")
        time.sleep(1)

# def connect_database():
#     semaphore.acquire()
#     print(f"Database connect with thread {threading.current_thread().name}")
#     print(f"Database operate with thread {threading.current_thread().name}")
#     time.sleep(3)
#     print(f"Database disconnect with thread {threading.current_thread().name}")
#     time.sleep(1)
#     semaphore.release()
#     semaphore.release()


threads = []

for i in range(1, max_connections * 2):
    thread = threading.Thread(target=connect_database, name=f"thr {i}")
    threads.append(thread)
    thread.start()


for thread in threads:
    thread.join()