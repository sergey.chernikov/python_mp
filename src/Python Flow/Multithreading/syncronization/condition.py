import threading, time

condition = threading.Condition(lock=threading.Lock())
max_threads = 5
counter_condition = 5
number_threads_to_notify = 2

counter = 0


def producer_condition():
    global counter
    while True:
        with condition:
            counter += 1
            print(f"Incrementing counter {counter}")
            condition.notify(number_threads_to_notify)
        time.sleep(1)
        if counter >= counter_condition + max_threads:
            break


def consumer_condition():
    print(f"Consumer {threading.current_thread().name} is created")
    with condition:
        result = condition.wait_for(lambda: counter >= counter_condition)
        print(f"{threading.current_thread().name}: counter is now {counter}, condition satisfied.")
        # if result:
        #     print(f"{threading.current_thread().name}: counter is now {counter}, condition satisfied.")
        # else:
        #     print("{threading.current_thread().name}: wait_for timed out.")


consumers = []

for i in range(0, max_threads + 1):
    consumer = threading.Thread(target=consumer_condition, name=f"cons -  {i}")
    consumers.append(consumer)
    consumer.start()

producer = threading.Thread(target=producer_condition)
producer.start()

for consumer in consumers:
    consumer.join()

producer.join()
