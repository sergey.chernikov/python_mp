import asyncio, time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from functools import partial

def just_simple_delay(delay):
    print('Start blocking part')
    time.sleep(delay)
    print('End blocking part')
    return delay

async def async_delay_in_parallel():
    for i in range(1,11):
        await asyncio.sleep(1)
        print(f'I am doing something in parallel {i}')
async def main():
    loop = asyncio.get_running_loop()
    task = loop.create_task(async_delay_in_parallel())
    start_time = time.time()
    with ProcessPoolExecutor() as executor:
        result = await loop.run_in_executor(executor, partial(just_simple_delay, 10))
    await task
    end_time = time.time()
    print(f'just_simple_delay: {end_time - start_time:.2f}')

if __name__ == '__main__':
    asyncio.run(main())