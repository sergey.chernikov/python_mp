import threading, time

max_threads = 5

def cpu_bound_operation(x=0):
    return sum(i*i for i in range(x))

def measure_time(func, *args):
    start_time = time.time()
    func(*args)
    end_time = time.time()
    print(f"Time taken by {func.__name__}: {end_time - start_time:.2f} seconds")


x = 10**7

start_time = time.time()
cpu_bound_operation(x)
end_time = time.time()

print(f"Time taken by cpu_bound_operation : {end_time - start_time:.2f} sec")

thread_pool = []

for i in range(0, max_threads):
    thread_pool.append(threading.Thread(target=cpu_bound_operation, args=(x,), name=f"Thread - {i}"))



start_time = time.time()
for thread in thread_pool:
    thread.start()

for thread in thread_pool:
    thread.join()

end_time = time.time()
print(f"Time taken by multi-threading (cpu_bound_operation): {end_time - start_time:.2f} sec")