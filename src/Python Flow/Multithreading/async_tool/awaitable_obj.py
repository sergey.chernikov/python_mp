import asyncio


class CustomAwaitable:
    def __init__(self):
        self.wait = 5
        self.value = 10

    def __await__(self):
        print('Enter [__await__]')
        print('Yield  [__await__]')
        #yield from asyncio.sleep(self.wait).__await__()
        yield 1
        yield 2
        print('Exit  [__await__]')
        return self.value


async def main():
    print('Create [CustomAwaitable]')
    awaitable = CustomAwaitable()
    print('Wait for result [CustomAwaitable]')
    result = await awaitable
    print(f'Received result [CustomAwaitable] {result}')


asyncio.run(main())
