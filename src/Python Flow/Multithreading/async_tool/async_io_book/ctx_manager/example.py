import asyncio


class AsyncContextManager:
    async def __aenter__(self):
        print('[AsyncContextManager.__aenter__] entering context ')

    async def __aexit__(self, exc_type, exc, tb):
        print('[AsyncContextManager.__aexit__] exiting context')


async def test_ctx():
    print('[AsyncContextManager] Create manager')
    async with AsyncContextManager() as ctx_mng:
        print('Doing work...')
        await asyncio.sleep(5)
        print('The work is done')
    print('[AsyncContextManager] Destroy manager')


asyncio.run(test_ctx())
