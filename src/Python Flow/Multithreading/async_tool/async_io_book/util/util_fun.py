from asyncio import sleep

import functools
import time
from typing import Callable, Any
def async_timed():
    def wrapper(func: Callable) -> Callable:
     @functools.wraps(func)
     async def wrapped(*args, **kwargs) -> Any:
         print(f'выполняется {func} с аргументами {args} {kwargs}')
         start = time.time()
         try:
            return await func(*args, **kwargs)
         finally:
             end = time.time()
             total = end - start
             print(f'{func} завершилась за {total:.4f} с')
         return wrapped
    return wrapper


async def delay(delay: int = 0, name: str = "") -> int:
    print(f'[{name}] Started execution')
    await sleep(delay)
    print(f'[{name}] Finished execution in {delay} sec')
    return delay

async def printer(delay: int = 0, max_iterations: int = 10, name: str = "") -> int:
    print(f'[{name}] Started execution')
    counter = 0
    while True:
        counter += 1
        if counter > max_iterations:
            break
        await sleep(delay)
        print(f'[{name}] I am working, ha ha')
    print(f'[{name}] Finished execution in {delay} sec')

def execution_time_measure(fun):
    async def wrapper(*args, **kwargs):
        start_time = time.time()
        print(f"[{fun.__name__}] Start time: {start_time}")
        rs = await fun(*args, **kwargs)
        end_time = time.time()
        print(f"[{fun.__name__}] End time: {end_time}")
        print(f"[{fun.__name__}] Execution time is: {end_time - start_time : .3f} ms")
        return rs