import asyncio, time
from util import delay, printer


def call_later():
    print("Меня вызовут в ближайшем будущем!")


async def main():
    start_time = time.time()
    db_result = asyncio.create_task(delay(5, "database_call"))
    api_result = asyncio.create_task(delay(5, "api_call"))
    pr_result = asyncio.create_task(printer(1, 10, "printer_call"))
    try:
        loop = asyncio.get_running_loop()
        loop.call_soon(call_later)
        await asyncio.wait_for(pr_result, timeout=8)
        await asyncio.wait_for(api_result, timeout=8)
        await asyncio.wait_for(db_result, timeout=8)


    except asyncio.exceptions.TimeoutError as e:
        print('A task has been canceled', e)
    end_time = time.time()
    print(f"Execution time is: {end_time - start_time : .2f} ms")
    return db_result.result() + api_result.result()


def execution_time_measure(fun):
    async def wrapper(*args, **kwargs):
        start_time = time.time()
        print(f"[{fun.__name__}] Start time: {start_time}")
        rs = await fun(*args, **kwargs)
        end_time = time.time()
        print(f"[{fun.__name__}] End time: {end_time}")
        print(f"[{fun.__name__}] Execution time is: {end_time - start_time : .3f} ms")
        return rs

    return wrapper

@execution_time_measure
async def cpu_bound_work() -> int:
    counter = 0
    for i in range(10000000):
        counter = counter + 1
    return counter


result = asyncio.run(cpu_bound_work(), debug=True)

# loop = asyncio.new_event_loop()
# try:
#  loop.run_until_complete(main())
# finally:
#  loop.close()


print(result)
