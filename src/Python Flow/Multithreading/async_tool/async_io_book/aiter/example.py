import asyncio


class AsyncRange:
    """An asynchronous iterator to simulate asynchronous range function."""
    def __init__(self, start, end, step=1):
        self.current = start
        self.end = end
        self.step = step

    def __aiter__(self):
        return self

    async def __anext__(self):
        if self.current < self.end:
            result = self.current
            self.current += self.step
            await asyncio.sleep(1)
            return result
        else:
            raise StopAsyncIteration


async def main():
    async for number in AsyncRange(0, 5):
        print(number)


asyncio.run(main())