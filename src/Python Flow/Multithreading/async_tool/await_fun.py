import asyncio
async def sleep(delay):
    print(f'I am started with wait time: {delay} sec')
    await asyncio.sleep(delay)
    print(f'I am waited: {delay} sec')
    return delay



async def main():
    tasks = [asyncio.create_task(sleep(i)) for i in range(5, 7)]
    done, pending = await asyncio.wait(tasks, timeout=None, return_when=asyncio.FIRST_COMPLETED)

    for task in done:
        print(f'Done task result: {await task}')
    for task in pending:
        print(f'Pending task was not complete: {task}')

asyncio.run(main())