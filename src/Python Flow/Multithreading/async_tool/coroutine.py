import asyncio


async def fetch_data_coroutine_function(name):
    await asyncio.sleep(3)  # stops here like generator and remember the state
    rs = {
        'username': 'user',
        'password': '==%#DEJDIEJIODENE&&#@#478387832794273947'
    }
    print(f"Task {name} is executed")
    return rs


async def main_create_task():  # with create task
    asyncio.create_task(fetch_data_coroutine_function("test_1"))
    asyncio.create_task(fetch_data_coroutine_function("test_2"))
    coroutine_obj = asyncio.create_task(fetch_data_coroutine_function("test_3"))
    rs = await coroutine_obj  # will stop here until the function is executed due to 'await'
    print(f'Done with: {rs}')


async def main_gather():  # with gather
    gather_obj = asyncio.gather(
        # gather have no error handling, and it will not cancel other coroutine automatically if one of them is failed
        fetch_data_coroutine_function("test_1"),
        fetch_data_coroutine_function("test_2"),
        fetch_data_coroutine_function("test_3"),
        return_exceptions=True # will return exceptions, before 3.7 -> if False, than gather_obj.cancel() will NOT cancel tasks in the case of an exception
    )

    results = await gather_obj

    for result in results:
        print(f'Done with: {result}')


async def main_task_group():  # provides built in error handling that will automatically cancel other tasks in the case of an exception
    tasks = []
    async with asyncio.TaskGroup() as tg:
        for i in range(1, 4):
            task = tg.create_task(fetch_data_coroutine_function(f"test_{i}"))
            tasks.append(task)

    results = [task.result() for task in tasks]
    for result in results:
        print(f'Done with: {result}')


async def main_task_manual():  # provides built in error handling that will automatically cancel other tasks in the case of an exception
    event_loop = asyncio.get_running_loop()
    future = event_loop.create_future()

    task_1 = asyncio.create_task(fetch_data_coroutine_function("test_1"))
    result = await task_1
    print(result)


async def manual_set_future_result(future, callback):
    result = await future
    future.set_result()


asyncio.run(main_task_manual())  # creates event loop add all coroutines there and starts execution

el = asyncio.get_running_loop()
el.run_until_complete(future)
el.run_until_complete(future)