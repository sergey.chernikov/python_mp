import asyncio


async def set_after(fut, delay, value):
    await asyncio.sleep(delay)
    # fut.set_exception(Exception('THat is a timeslot to debug'))
    fut.set_result(value)

async def main():
    fut = asyncio.Future() # creating, waiting, ready or cancel conditions
    just_a_callback = lambda fut: print(f'I am done. PS: Your future: {fut.result()}')
    fut.add_done_callback(just_a_callback)
    #fut.remove_done_callback(just_a_callback)
    await set_after(fut, 1, '... world')

    print('hello ...')
    print(fut.result()) # will resolve with result or with exception if not successfully resolved
    print(f'Resolved: {fut.done()}') # is resolved?

asyncio.run(main())

asyncio.run