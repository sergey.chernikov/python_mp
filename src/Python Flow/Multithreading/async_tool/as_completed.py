import asyncio
from random import randint


async def call_mock_rest_get(sleep, id):
    await asyncio.sleep(sleep)
    return {
        'name': id,
        'executed in': sleep
    }


async def main():
    tasks = []
    for i in range(0,10):
        task = asyncio.create_task(call_mock_rest_get(randint(1, 12), i))
        tasks.append(task)


    results = asyncio.as_completed(tasks, timeout=10)


    for result in results:
        try:
            print(await result)
        except asyncio.TimeoutError as e:
            print('Upssss, timeout')


asyncio.run(main())
