import threading, time

def task(name, sema, prev_sema):
    with prev_sema:  # Ожидание разрешения от предыдущего семафора
        print(f"Поток {name} начал работу.")
        time.sleep(2)  # Имитация продолжительной работы
        print(f"Поток {name} завершил работу.")
        sema.release()  # Выдача разрешения следующему потоку

# Создание семафоров
sema1 = threading.Semaphore(0)
sema2 = threading.Semaphore(0)
sema3 = threading.Semaphore(0)

# Последний семафор изначально разрешен, чтобы стартовать цепочку
start_sema = threading.Semaphore(1)

threads = [
    threading.Thread(target=task, args=("1", sema1, start_sema)),
    threading.Thread(target=task, args=("2", sema2, sema1)),
    threading.Thread(target=task, args=("3", sema3, sema2)),
    threading.Thread(target=task, args=("4", start_sema, sema3)),  # Для замыкания цепочки на начало
]

# Запуск всех потоков
for t in threads:
    t.start()

# Ожидание окончания всех потоков
for t in threads:
    t.join()

print("Все потоки завершили работу.")