import threading


def print_numbers(sync_event, next_event, start, max_num=100):
    for number in range(start, max_num + 1, 2):
        sync_event.wait()
        sync_event.clear()
        print(f"{threading.current_thread().name}: {number}")
        next_event.set()


even_event = threading.Event()
odd_event = threading.Event()

thr_print_even = threading.Thread(target=print_numbers, args=(even_event, odd_event, 0,), name="Even")
thr_print_odds = threading.Thread(target=print_numbers, args=(odd_event, even_event, 1,), name="Odd")

thr_print_even.start()
thr_print_odds.start()

even_event.set()

thr_print_even.join()
thr_print_odds.join()