from multiprocessing import Process, Pipe, current_process


parent, child = Pipe()
def parent_process(connection):
    print(f"[{current_process().name}] Started process")
    print(f"[{current_process().name}] Use IPC (PIPE) to send data parent process ")
    for i in range(0, 3):
        data = {f'data_{i}': f'data from {current_process().name}'}
        print(f"[{current_process().name}] Sent: {data}")
        connection.send(data)
    connection.send(None) # End signal
    print(f"[{current_process().name}] Stopped process ")

def child_process(connection):
    print(f"[{current_process().name}] Started process ")
    print(f"[{current_process().name}] Use IPC (PIPE) to receive data parent process ")
    while True:
        data = connection.recv()
        if data is None:
            break
        print(f"[{current_process().name}] Received: {data}")
    print(f"[{current_process().name}] Stopped process ")


if __name__ == '__main__':
    parent = Process(target=parent_process, args=(parent,), name="Parent")
    child = Process(target=child_process, args=(child,), name="Child")

    child.start()
    parent.start()

    child.join()
    parent.join()