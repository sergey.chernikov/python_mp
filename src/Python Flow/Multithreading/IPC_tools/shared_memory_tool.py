from multiprocessing import shared_memory

name = "TestSharedMemory"

s_mem = shared_memory.SharedMemory(create=True, size=10, name=name)
s_mem.buf[:4] = bytearray([12, 11, 10,11])
s_mem.buf[2] = 50

print(s_mem)

existing_shm = shared_memory.SharedMemory(name=name)

print(existing_shm)
print(existing_shm.buf[0])
print(existing_shm.buf[2])

s_mem.close()
print(existing_shm.buf[2])
