import socket, threading, multiprocessing
import time

event = threading.Event()


HOST = '127.0.0.1'
PORT = 8169


def server_client(conn, addr):
    print(f'[Server] [Connection from {addr}]')
    try:
        while True:
            data = conn.recv(1024)
            if not data:
                break
            print(f'[Server] received [{addr}]: {data.decode()}')

    finally:
        conn.close()
def server(host: str, port: int):
    with  socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind((host, port))
        server_socket.listen()
        print(f'[Server] started')
        while True:
            conn, addr = server_socket.accept()
            client_thread = threading.Thread(target=server_client, args=(conn, addr))
            client_thread.start()
        print(f'[Server] stopped')



def client(host: str, port: int):
    time.sleep(5)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        for i in range(0,3):
            message = f"Client ({multiprocessing.current_process().name}) message number: {i}".encode()
            print(f"[Client {multiprocessing.current_process().name}] Sending message: {message}")
            s.sendall(message)
            time.sleep(2)
        s.close()
        print(f"[Client {multiprocessing.current_process().name}] Bye Bye")


server_process = multiprocessing.Process(target=server, args=(HOST, PORT))
client_one_process = multiprocessing.Process(target=client, args=(HOST, PORT), name='client one')
client_two_process = multiprocessing.Process(target=client, args=(HOST, PORT), name='client two')

if __name__ == '__main__':
    server_process.start()
    client_one_process.start()
    client_two_process.start()

    server_process.join()
    client_one_process.join()
    client_two_process.join()