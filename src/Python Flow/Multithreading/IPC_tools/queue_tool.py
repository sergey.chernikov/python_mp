from multiprocessing import Process, Queue, current_process


queue = Queue()
def parent_process(queue):
    print(f"[{current_process().name}] Started process")
    print(f"[{current_process().name}] Use IPC (PIPE) to send data parent process ")
    for i in range(0, 3):
        data = {f'data_{i}': f'data from {current_process().name}'}
        print(f"[{current_process().name}] Sent: {data}")
        queue.put(data)
    queue.put(None)
    print(f"[{current_process().name}] Stopped process ")

def child_process(queue):
    print(f"[{current_process().name}] Started process ")
    print(f"[{current_process().name}] Use IPC (PIPE) to receive data parent process ")
    while True:
        data = queue.get()
        if data is None:
            break
        print(f"[{current_process().name}] Received: {data}")
    print(f"[{current_process().name}] Stopped process ")


if __name__ == '__main__':
    parent = Process(target=parent_process, args=(queue,), name="Parent")
    child = Process(target=child_process, args=(queue,), name="Child")

    child.start()
    parent.start()

    child.join()
    parent.join()