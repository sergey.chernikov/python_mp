import threading, time,os

max_threads = 5
out_dir = "./test_res"

os.mkdir(out_dir)
def io_bound_operation(x=0):
    with open(f"{out_dir}/{threading.current_thread().name}", "w") as file:
        for i in range(0, x):
            file.write(threading.current_thread().name)


x = 10**2

start_time = time.time()
io_bound_operation(x)
end_time = time.time()

print(f"Time taken by io_bound_operation : {end_time - start_time:.2f} sec")

thread_pool = []

for i in range(0, max_threads):
    thread_pool.append(threading.Thread(target=io_bound_operation, args=(x,), name=f"Thread - {i}"))



start_time = time.time()
for thread in thread_pool:
    thread.start()
    thread.join()

end_time = time.time()
print(f"Time taken by multi-threading (io_bound_operation): {end_time - start_time:.2f} sec")

for file in os.listdir(out_dir):
    os.remove(f"{out_dir}/{file}")
os.rmdir(out_dir)