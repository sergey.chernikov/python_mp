arr_1 = [1,2,3,4,5,6,7,8]
arr_2 = arr_1[:]

print(id(arr_1))
print(id(arr_2))
print(id(arr_1) == id(arr_2))