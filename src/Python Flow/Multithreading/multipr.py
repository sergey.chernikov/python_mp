from multiprocessing import Process
from time import sleep
def infinite_function():
    while True:
        print("My name is process")
        sleep(1)

if __name__ == '__main__':
    p = Process(target=infinite_function)
    p1= Process(target=infinite_function)

    p.start()
    p1.start()
    print(p.pid)
    print(p1.pid)
    p.join()
    p1.join()


import threading

condition = threading.Condition(lock=threading.Lock())


def print_numbers(shared_counter, is_even, max_counter=100):
    while True:
        with condition:
            while not shared_counter[0] % 2 != is_even:
                condition.wait()
            print(f"{threading.current_thread().name}: {shared_counter[0]}")
            shared_counter[0] += 1
            condition.notify()
            if shared_counter[0] >= max_counter:
                return


shared_counter = [0]
thr_print_even = threading.Thread(target=print_numbers, args=(shared_counter, True), name="Even")
thr_print_odds = threading.Thread(target=print_numbers, args=(shared_counter, False), name="Odd")

thr_print_even.start()
thr_print_odds.start()

thr_print_even.join()
thr_print_odds.join()
