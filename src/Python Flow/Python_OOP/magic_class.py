"""
- Write a class which creates obj. Class must implement:
a) __getattribute__ checks if attr name  == "hello" -> return "hello world" else -> default behavior
b) __getattr__ checks if attr name  == "love" -> return "love you" else -> return None
c) __setattr__ checks if attr name  == "vasia" -> assign obj.name = "pupkin" else -> assign obj.name = value
d) write test to prove if everything works

"""

import unittest


class MagicClass:
    def __getattribute__(self, item):
        if item == "hello":
            return "hello world"
        return super().__getattribute__(item)

    def __getattr__(self, item):
        if item == "love":
            return "love you"
        return None

    def __setattr__(self, key, value):
        if key == "vasia":
            super().__setattr__(key, "pupkin")
        else:
            super().__setattr__(key, value)


class TestMagicClass(unittest.TestCase):
    def setUp(self):
        self.magic_object_ins = MagicClass()
        self.magic_object_ins.test_attribute = "test_attribute"
        self.magic_object_ins.hello = "test_hello"
        self.magic_object_ins.vasia = "vasia"

    def test_getattribute_if_name_is_hello_returns_hello_world(self):
        self.assertEqual("hello world", self.magic_object_ins.hello)

    def test_getattribute_if_name_is_test_attribute_returns_test_attribute(self):
        self.assertEqual("test_attribute", self.magic_object_ins.test_attribute)

    def test_getattribute_if_attr_doesnt_exist_but_name_is_love_returns_love_you(self):
        self.assertEqual("love you", self.magic_object_ins.love)

    def test_getattr_if_attr_doesnt_exist_returns_none(self):
        self.assertIsNone(self.magic_object_ins.no_attribute)

    def test_setattr_if_name_is_vasia_set_pupkin(self):
        self.assertEqual("pupkin", self.magic_object_ins.vasia)

    def test_setattr_if_any_name_is_set(self):
        self.magic_object_ins.atrr_set = "atrr_set"
        self.assertEqual("atrr_set", self.magic_object_ins.atrr_set)

if __name__ == "__main__":
    TestMagicClass()
