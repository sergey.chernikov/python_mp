class Number:
    def __init__(self, weight: int = 0):
        self.weight = weight

    def __add__(self, other):
        if type(other) is not int:
            return NotImplemented
        return self.weight + other

class NumberBrother(Number):

    def __radd__(self, other):
        if type(other) is not Number:
            return NotImplemented
        return self.weight + other.weight



import unittest
class TestNumber(unittest.TestCase):
    def test_add_int(self):
        expected = 60
        actual = Number(10) + 50
        self.assertEqual(expected, actual)

    def test_add_number_brother_to_number(self):
        expected = 30
        actual = Number(10) + NumberBrother(20)
        self.assertEqual(expected, actual)

    def test_add_number_brother_to_number_raise(self):
        with self.assertRaises(Exception) as context:
            NumberBrother(20) + Number(10)
        expected = "unsupported operand type(s)"
        actual = context.exception.args[0]
        self.assertTrue(expected in actual)
    def test_add_not_int_raise(self):
        with self.assertRaises(Exception) as context:
            Number(10) + 55.55
        expected = "unsupported operand type(s)"
        actual = context.exception.args[0]
        self.assertTrue (expected in actual)

    def test_add_int_to_number_raise(self):
        with self.assertRaises(TypeError) as context:
            111 + Number(10)
        expected = "unsupported operand type(s)"
        actual = context.exception.args[0]
        self.assertTrue(expected in actual)
