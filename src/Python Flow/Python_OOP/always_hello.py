'''
- Write a class, instances of which works in the following way:
  obj.fun() # prints Hello!
  obj.aaaaa() # prints Hello!
  obj.any_name_you_imagine() # prints Hello!
  obj.__doc__ # all attributes starting with underscore left untoched
'''
import unittest


class AlwaysSayHello:
    def __getattribute__(self, item):
        if item.startswith('_'):
            return super().__getattribute__(item)
        return lambda: print("Hello")


class TestAllwaysHello(unittest.TestCase):
    def test_if_any_method_return_callable(self):
        cl = AlwaysSayHello()
        self.assertTrue(callable(cl.efsfsttete3243))

    def test_if_any_set_attribute_is_callable(self):
        cl = AlwaysSayHello()
        cl.atrrr = "werwer"
        self.assertTrue(callable(cl.atrrr))
    def test_underscore(self):
        cl = AlwaysSayHello()
        cl._atr = "werwer"
        self.assertTrue(isinstance(cl._atr, str))

    def test_double_underscore(self):
        cl = AlwaysSayHello()
        cl.__atr = "werwer"
        self.assertTrue(isinstance(cl.__atr, str))