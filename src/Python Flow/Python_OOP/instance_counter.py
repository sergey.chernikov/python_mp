"""
    - Write a class which counts the number of created instances
    of this class with method which shows this number.
    All derived classes must count only their instances
"""
import unittest
from collections import defaultdict


class INSCounter:
    _ins_counter = defaultdict(int)

    def __init__(self):
        type(self)._ins_counter[type(self)] += 1

    @classmethod
    def get_inst_count(cls):
        return cls._ins_counter.get(cls, 0)


class INSCounterChild(INSCounter):
    pass


class INSCounterPublicChild(INSCounter):
    pass
class TestInstCounter(unittest.TestCase):
    def setUp(self):
        self.expected_inst_counter_num = 8
        self.expected_inst_counter_child_num = 10
        self.expected_inst_counter_public_child_num = 12

        for _ in range(self.expected_inst_counter_num):
            INSCounter()
        for _ in range(self.expected_inst_counter_child_num):
            INSCounterChild()
        for _ in range(self.expected_inst_counter_public_child_num):
            INSCounterPublicChild()

    def test_if_method_isn_counter_works(self):
        self.assertEqual(self.expected_inst_counter_num, INSCounter.get_inst_count())
        self.assertEqual(self.expected_inst_counter_child_num, INSCounterChild.get_inst_count())
        self.assertEqual(self.expected_inst_counter_public_child_num, INSCounterPublicChild.get_inst_count())

if __name__ == "__main__":
    TestInstCounter()