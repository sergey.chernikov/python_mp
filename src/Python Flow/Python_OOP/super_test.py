class Root:
    def say(self):
        print(f'I am Root')


class FirstRootChild(Root):
    def say(self):
        super().say()
        print(f'I am FirstRootChild')

class SecondRootChild(Root):
    def say(self):
        super().say()
        print(f'I am SecondRootChild')

class ThirdRootChild(Root):
    def say(self):
        super().say()
        print(f'I am ThirdRootChild')


class IamYourCommonChild(FirstRootChild, SecondRootChild, ThirdRootChild):
    def say(self):
        super().say()
        print(f'I am IamYourCommonChild')



import unittest
class TestMRO(unittest.TestCase):
    def test_me(self):
        root = IamYourCommonChild()
        root.say()
        self.assertEquals(True, True)