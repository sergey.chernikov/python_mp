class A:
    def say_who_iam(self):
        print(f"I am A")

class B(A):
    def say_who_iam(self):
        super().say_who_iam()
        print("I am B")

class D(A):
    def say_who_iam(self):
        super().say_who_iam()
        print("I am D")

class C(B,D):
    def say_who_iam(self, a=0):
        super().say_who_iam()
        print("I am C")

c = C()
c.say_who_iam(2)
print(help(C))