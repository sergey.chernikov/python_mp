"""
  - Write a function wich takes 2 arguments: arg1 and arg2 which are classes.
  For all normal methods only in this classes which names start with 'fun_' swap them between classes
  ( for example, method fun_1() in A should be moved to fun_1() in B and vice versa, etc).
  Test your program with assert
"""

import types


class A:
    def fun_1(self):
        return "I as method of A"

    def func_1(self):
        return "I as method of A"

    @staticmethod
    def fun_tastic():
        return "I am not normal method, must not be swapped A"


class B:
    def fun_1(self):
        return "I as method of B"

    def func_1(self):
        return "I as method of B"


def get_attributes(obj_type, filter):
    return {
        key: val for key, val in obj_type.__dict__.items()
        if isinstance(getattr(obj_type(), key), types.MethodType)
           and callable(val)
           and key.startswith(filter)
    }


def remove_attributes(obj_type, attributes):
    for key, val in attributes.items():
        delattr(obj_type, key)


def set_attributes(obj_type, attributes):
    for key, val in attributes.items():
        setattr(obj_type, key, val)


def swap_methods(arg1, arg2):
    arg1_methods = get_attributes(arg1, "fun_")
    arg2_methods = get_attributes(arg2, "fun_")
    remove_attributes(arg1, arg1_methods)
    remove_attributes(arg2, arg2_methods)
    set_attributes(arg1, arg2_methods)
    set_attributes(arg2, arg1_methods)


swap_methods(A, B)
a = A()
b = B()

assert a.fun_1() == "I as method of B"
assert a.fun_1() == "I as method of B"
assert a.fun_tastic() == "I am not normal method, must not be swapped A"
assert b.fun_1() == "I as method of A"
assert b.func_1() == "I as method of B"
assert a.fun_tastic() == "I am not normal method, must not be swapped A"
