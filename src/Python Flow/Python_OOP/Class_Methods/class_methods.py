import unittest
from abc import ABC, abstractmethod


class TestClass:
    __instance = None
    def __init__(self, omega: str):
        self.omega: str = omega

    @classmethod
    def class_method_as_different_constructor(cls, alpha: str):
        return cls(alpha * 2)

    @classmethod
    def instance(cls, alpha: str):
        if cls.__instance == None:
            cls.__instance = cls(alpha * 2)
        return cls.__instance

    @classmethod
    def class_method_as_class_attribute(cls):
        cls.MAIN_LIMIT = 12
        return cls.MAIN_LIMIT

    @staticmethod
    def validate_constructor_params(a: int, b: int):
        return a + b

    def instance_method_get_omega(self):
        return self.omega

ts = TestClass.class_method_as_class_attribute()

class ChildOFTestClass(TestClass):
    pass

class AbstractClass:
    def abstract_method(self) -> str:
        print(10)
class ChildOfAbstractClass(AbstractClass):
    def abstract_method(self) -> str:
        return ...

class SuperChildOfAbstractClass(AbstractClass):
    def abstract_method(self) -> str:
        super().abstract_method()

cls = ChildOfAbstractClass()
cls.abstract_method()
#
# class TestAll(unittest.TestCase):
#     omega = "omega"
#     test_class_instance = TestClass(omega)
#
#     def test_abstract_class_creation(self):
#         with self.assertRaises(Exception) as context:
#             AbstractClass()
#         self.assertTrue("Can't instantiate abstract class AbstractClass" in context.exception.args[0])
#
#         child = ChildOfAbstractClass()
#         super_child = SuperChildOfAbstractClass()
#
#         self.assertEqual("child", child.abstract_method())
#         self.assertEqual(None, super_child.abstract_method())
#
#     def test_class_method(self):
#         actual = TestClass.class_method_as_class_attribute()
#         expected = TestClass.MAIN_LIMIT
#         self.assertEqual(expected, actual)
#
#         del TestClass.MAIN_LIMIT
#
#         actual = self.test_class_instance.class_method_as_class_attribute()
#         expected = TestClass.MAIN_LIMIT
#         self.assertEqual(expected, actual)
#
#     def test_static_method(self):
#         a, b = 1, 3
#         expected = a + b
#         actual = self.test_class_instance.static_method_sum(a, b)
#         self.assertEqual(expected, actual)
#
#         actual = TestClass.static_method_sum(a, b)
#         self.assertEqual(expected, actual)
#
#     def test_instance_method(self):
#         expected = self.omega
#         actual = self.test_class_instance.instance_method_get_omega()
#         self.assertEqual(expected, actual)
#
#         actual = TestClass.instance_method_get_omega(self.test_class_instance)
#         self.assertEqual(expected, actual)

