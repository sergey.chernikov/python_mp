from functools import partial

class StaticMethod(object):
    def __init__(self, fun):
        self.fun = fun

    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            print("function is called")
            return self.fun(self, *args, **kwargs)
        return wrapper





class ClassMethod(object):
    def __init__(self, method):
        self.method = method

    def __get__(self, obj, cls=None):
        if cls is None:
            cls = type(obj)
        def wrapper(*args, **kwargs):
            return self.method(cls, *args, **kwargs)

        return wrapper
class ClassWithStaticMethod:

    @StaticMethod
    def static_method(a,b):
        return a + b

print(ClassWithStaticMethod.static_method(3, 4))
