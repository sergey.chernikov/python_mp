from functools import wraps


def log_decorator_with_wraps(func):
    @wraps(func)
    def wrapper():
        print(f'Вызов функции: {func.__name__}')
        func()

    return wrapper

def log_decorator_without_wraps(func):
    def wrapper():
        print(f'Вызов функции: {func.__name__}')
        func()

    return wrapper

@log_decorator_with_wraps
def hello_world():
    print("Hello, world!")



print(hello_world.__name__)

@log_decorator_without_wraps
def hello_world():
    print("Hello, world!")

print(hello_world.__name__)