class StrIter:
    def __init__(self, val):
        self.__val = val
        self.__idx = -1
    def __iter__(self):
        self.__idx += 1
        yield from ((inx, elem) for inx, elem in enumerate(self.__val))

    #def __next__(self):
    #    self.__idx += 1
    #    return (self.__idx, (self))

stit = StrIter({"1":"1","2":"2"})
for idnx, val in stit:
    print(idnx, val)