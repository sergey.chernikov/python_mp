from typing import Iterable, Iterator
from collections import defaultdict


class CounterIterator:
    def __init__(self, start=0, stop=0):
        self.start = start
        self.stop = stop
        self.current = start

    def __iter__(self):
      print("Just return self")
      return self
    def __next__(self):  # object is Iterator
        if self.current == self.stop:
            print("I am iterator and I don't want to work, I am exhausted, StopIteration exception is for you ;)")
            raise StopIteration
        self.current += 1
        print(f"I am iterator because I know who is the __next__ here: {self.current}")
        return self.current


print("======================= Proof that object is an Iterator ============================")
counter = iter(CounterIterator(0, 10))
next(counter)
next(counter)
next(counter)
next(counter)
next(counter)
next(counter)
list
counter = iter(CounterIterator(0, 10))
while True:
    try:
        next(counter)
    except StopIteration:
        print("done")


class CounterIterable:
    def __init__(self, start=0, stop=0):
        self.start = start
        self.stop = stop

    def __iter__(self):  # object is iterable
        print("Hi, I am iterable because I have __iter__ attribute")
        # return (i for i in range(self.start, self.stop + 1))
        return CounterIterator(self.start, self.stop)


print("======================= Proof that object is an Iterable ============================")
for i in CounterIterable(0, 10):
    pass