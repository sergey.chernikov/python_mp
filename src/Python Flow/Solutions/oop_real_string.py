class RealString:
    def __init__(self, val: str):
        self.val = val

    def __gt__(self, other):
        return len(self.__val) > len(other.val)

    def __le__(self, other):
        return len(self.val) < len(other.val)

    def __eq__(self, other):
        return len(self.val) == len(other.val)



print(RealString("Молоко") == RealString("Золото"))


def camel(val: str) -> str:
    result = ""
    is_prev_capital = False
    for char in val:
        if char in [" ", "_", "."]:
            result += char
            continue
        if  is_prev_capital is True:
            result += char.lower()
            is_prev_capital = False
        else:
            result += char.upper()
            is_prev_capital = True

    return result


print(camel("I am is Petia"))

import re
def shortener(s):
    result = ""
    skip_in_curl_brackets = False
    for char in s:
        if char == "(":
            skip_in_curl_brackets = True
        if char == ")":
            skip_in_curl_brackets = False
            continue
        if skip_in_curl_brackets:
            continue
        result += char
    return result



assert shortener("Дмитрий считает, что когда текст пишут в скобках (как вот тут, например), его читать не нужно.") == "Дмитрий считает, что когда текст пишут в скобках , его читать не нужно."