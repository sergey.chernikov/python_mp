import asyncio
from aiohttp import web
import datetime


async def handle_long_request(request):
    print("Handling long operation request...")
    # Имитация длительной операции I/O, например, запрос к внешнему API или базе данных
    await asyncio.sleep(5)
    return web.Response(text="Длительная операция завершена")


async def handle_fast_request(request):
    print("Handling fast operation request...")
    # Быстрая операция, например, возвращает текущее время
    current_time = datetime.datetime.now().isoformat()
    return web.Response(text=f"Текущее время: {current_time}")

async def get_routes():
    return [
        web.get('/long', handle_long_request),
        web.get('/fast', handle_fast_request),
        web.post('/fast', handle_fast_request)
    ]

async def start_server():
    app = web.Application()
    app.add_routes(await get_routes())

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 8080)
    await site.start()
    print("Server started at http://localhost:8080")

    # Сервер будет работать пока его не остановить
    await asyncio.Event().wait()


if __name__ == '__main__':
    asyncio.run(start_server())