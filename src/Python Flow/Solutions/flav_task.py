

def last_person(person_count = None, k = None):
    i = 0
    result = None
    persons = [ i for i in range(1,person_count+1)]
    while True:
        persons = [ elem for elem in persons if elem not in persons[k-1:persons.__len__():k]]
        if persons.__len__() == 2:
            inx = 0
            for counter in range(k):
                if counter % 2 != 0:
                    inx = 0
                else:
                    inx = 1
            return persons[inx-1]



assert last_person(9,3) == 1