

def cleaned_str(val: str) -> str:
    result = ""
    for i in range(0, len(val)):
        try:
            is_next_backspace = val[i + 1] == "@"
        except Exception:
            is_next_backspace = False
        if not is_next_backspace and val[i] != '@':
            result += val[i]
    return result

actual = cleaned_str("гр@оо@лк@оц@ва")
expected = "голова"

assert expected == actual