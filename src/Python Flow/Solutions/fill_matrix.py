
def fill_matrix(n=0, m=0):
    class DirectionEnum:
        left = "left"
        right = "right"
        up = "up"
        down = "down"

    matrix = []
    number = 0
    for i in range(1, n + 1):
        curr_line = []
        for j in range(1, m + 1):
            curr_line.append(0)
        matrix.append(curr_line)
    i, j, i_min, j_min, i_max, j_max = 0, 0, 0, 0, n-1, m-1
    direction = "" #up, down, left
    direction_count = 0
    while True:
        number += 1
        if number > n * m:
            break
        matrix[i][j] = number
        if i == i_min+1 and j == j_min and direction_count == 4:
            i_max -= 1
            j_max -= 1
            i_min += 1
            j_min += 1
            i = i_min
            j = j_min - 1
            direction = DirectionEnum.right
            direction_count = 1

        if j == j_max and direction != DirectionEnum.down:
            direction = DirectionEnum.down
            direction_count += 1

        if i == i_max and j == j_max and direction != DirectionEnum.left:
            direction = DirectionEnum.left
            direction_count += 1

        if i == i_max and j == j_min and direction != DirectionEnum.up:
            direction = DirectionEnum.up
            direction_count += 1

        if i == i_min and j == j_min and direction != DirectionEnum.right:
            direction = DirectionEnum.right
            direction_count += 1

        if direction == DirectionEnum.right:
            j += 1
        if direction == DirectionEnum.down:
            i += 1
        if direction == DirectionEnum.left:
            j -= 1
        if direction == DirectionEnum.up:
            i -= 1
    return matrix

matrix = fill_matrix(7,6)

print(" ========================================================== ")
for line in matrix:
    print(line)
print(" ========================================================== ")


assert matrix ==[
    [1,2,3,4,5,6],
    [22,23,24,25,26,7],
    [21,36,37,38,27,8],
    [20,35,42,39,28,9],
    [19,34,41,40,29,10],
    [18,33,32,31,30,11],
    [17,16,15,14,13,12]
]