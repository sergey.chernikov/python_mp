import sys
# from functools import partial
import importlib
import math as mathematical
# mathematical.sqrt(12)

#modules cache
print("Loaded modules:", sys.modules)


print(id(mathematical))
del sys.modules['math']

import math as mathematical

print(id(mathematical))

print(sys.builtin_module_names)
# finder locate th module, loader loads the module
for e in sys.meta_path:
    print(e)

print(type(mathematical))
print(f"Doc: {mathematical.__doc__}")
print(f"Loader: {mathematical.__loader__}")
print(f"Dict: {mathematical.__dict__}")
print(mathematical.__dict__['sqrt'](4))

print(dir(mathematical))


#manual import

import types

module_name = 'my_module.py'
module_file = 'my_module.py'
mod = types.ModuleType(module_name)
mod.__file__ = module_file

sys.modules[module_name] = mod

with open(module_file) as my_module:
    code = compile(
        my_module.read(),
        filename=module_file,
        mode='exec'
    )
    exec(code, mod.__dict__)
    mod.my_function() 


from mathopr import sum_any

print(sum_any(1, 2, 3, 4, 5, 6, 7, 8))