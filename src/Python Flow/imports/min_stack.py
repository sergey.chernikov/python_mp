class MinStack:
    def __init__(self):
        self.stack = []
        self.min_elem = None
        self.min_prev = None

    def get_min(self):
        return self.min_elem

    def push(self, elem):
        self.stack.append(elem)
        self.min_prev = self.min_elem
        if (self.min_elem is None
                or self.min_elem > elem):
            self.min_elem = elem

    def pop(self):
        pop_elem = self.stack.pop() if len(self.stack) > 0 else None
        if pop_elem == self.min_elem:
            self.min_elem = self.min_prev
        return pop_elem

    def top(self):
        return self.stack[-1]


min_stack = MinStack()

min_stack.push(12)
min_stack.push(17)
min_stack.push(13)
min_stack.push(15)
min_stack.push(19)
min_stack.push(9)
print(min_stack.pop())
print(min_stack.top())

print(min_stack.get_min())
