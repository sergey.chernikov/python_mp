from typing import List


class Solution:
    def canJump(self, nums: List[int]) -> bool:
        curr_position_val = 0
        pos = 0
        while len(nums) != pos:
            if curr_position_val <= 0:
                curr_position_val = nums[pos] - 1
                if curr_position_val == -1 and len(nums) > 1 and pos+1 != len(nums):
                    return False
            curr_position_val -= 1
            pos += 1
        return True

sol = Solution()

assert sol.canJump([2,3,1,1,4]) == True
assert sol.canJump([3,2,1,0,4]) == False
assert sol.canJump([0]) == True
assert sol.canJump([1]) == True
assert sol.canJump([2,0,0]) == True
assert sol.canJump([2,5,0,0]) == True