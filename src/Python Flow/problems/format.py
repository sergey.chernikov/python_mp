from typing import List

class Solution:
    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        blocks = []
        current_block = []
        length = 0
        for word in words:
            if len(word) > maxWidth:
                return []
            length += len(word) + 1
            if length <= maxWidth or length-1 <= maxWidth:
                current_block.append(word)
            else:
                blocks.append(current_block)
                current_block = []
                length = len(word) + 1
                current_block.append(word)
        else:
            if len(current_block) > 0:
                blocks.append(current_block)
        result = []
        for block in blocks:
            spaces_count = maxWidth - len("".join(block))
            if len(block) < 2:
                result.append(block[0] + str(" " * spaces_count))
                continue
            elif blocks.index(block) == (len(blocks) - 1):
                    line = " ".join(block) + " " * (spaces_count - 1)
                    result.append(line)
                    continue
            else:
                spaces_per_word = spaces_count // (len(block) - 1)
                spaces_in_addition = spaces_count % (len(block) - 1)
            final_line = ""
            is_first = True
            for elem in block:
                if is_first:
                    is_first = False
                    final_line += elem + str(" " * spaces_per_word.__ceil__())
                elif block.index(elem) == (len(block) - 1):
                    final_line += elem
                else:
                    final_line += elem + str(" " * spaces_per_word.__floor__())
            result.append(final_line)
        print(result)
        return result

sol = Solution()
assert (sol.fullJustify(["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], 20)
        == [
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
])
assert (sol.fullJustify(["What","must","be","acknowledgment","shall","be"], 16)
        == ["What   must   be",
            "acknowledgment  ",
            "shall be        "])

assert (sol.fullJustify(["This", "is", "an", "example", "of", "text", "justification."], 16)
        == ["This    is    an",
            "example  of text",
            "justification.  "])