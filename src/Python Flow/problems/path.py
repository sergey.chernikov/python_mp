class Solution:
    def simplifyPath(self, path: str) -> str:
        final_path = ""
        for path_elem in path.split("/"):
            if path_elem == "" or path_elem == ".":
                continue
            if path_elem == "..":
                path = final_path.split("/")
                path.pop()
                final_path = "/".join(path)
                continue
            final_path += "/" +path_elem
        return final_path if final_path else "/"

sol = Solution()

assert sol.simplifyPath("/.../a/../b/c/../d/./") == "/.../b/d"
assert sol.simplifyPath("/../") == "/"
assert sol.simplifyPath("/home/user/Documents/../Pictures") == "/home/user/Pictures"