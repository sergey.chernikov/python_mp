class MinStack:

    def __init__(self):
        self.__stack = []
        self.__min_stack = []

    def push(self, val: int) -> None:
        if len(self.__min_stack) < 1:
            self.__min_stack.append(val)
        elif val <= self.__min_stack[-1]:
            self.__min_stack.append(val)
        self.__stack.append(val)

    def pop(self) -> None:
        val = self.__stack.pop()
        if val == self.__min_stack[-1]:
            self.__min_stack.pop()
        return val

    def top(self) -> int:
        return self.__stack[-1]

    def getMin(self) -> int:
        return  self.__min_stack[-1]





obj = None

operations = ["MinStack","push","push","push","push","getMin","pop","getMin","pop","getMin","pop","getMin"]
values = [[],[2],[0],[3],[0],[],[],[],[],[],[],[]]
answers = []
for idx in range(0,len(operations)):
    operation, val = operations[idx], values[idx]
    if operation == "MinStack":
        obj = MinStack()
        continue
    if len(val) > 0:
        answers.append(eval(f'obj.{operation}({val})'))
    else:
        answers.append(eval(f'obj.{operation}()'))

print(answers)