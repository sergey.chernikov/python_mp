from Exceptions.exception import PythonFinder, PythonNotFound

try:
    pf = PythonFinder()
    pf.find_python()
except PythonNotFound as e:
    print(e)
except Exception as e:
    print(e)
else:
    print("No exception found")
finally:
    print("Will do it anyway")


try:
    raise AttributeError("AN Error")
except AttributeError:
    print("AttributeError")
except KeyError:
    print("KeyError")
except:
    print("Catch any error")
else:
    print("No exception")
finally:
    print("I will be executed anyway")


print("=================================================================")
try:
    try:
        raise Exception("My Exception")
    except Exception as e:
        print(e)
        raise ValueError from e
        raise ValueError from None
        raise

except Exception as e:
    print(e.__context__)  # None
    print(e.__cause__)    # None

print("=================================================================")





print("=================================================================")
for _ in None:
    print("for")
    raise Exception()
else:
    print("I DO")