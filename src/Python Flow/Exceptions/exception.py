class PythonNotFound(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)

class PythonFinder:
    def find_python(self):
        raise PythonNotFound("Cannot find Python")

    def find_other(self):
        raise Exception("Cannot find other")