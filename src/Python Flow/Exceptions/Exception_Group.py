try:
    raise ExceptionGroup("MultipleErrors", [ValueError("wrong value"), TypeError("wrong type")])
except* ValueError:
    print("ValueError")
except* TypeError:
    print("TypeError")