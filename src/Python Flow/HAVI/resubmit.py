import json, datetime, requests

with open("report.json", "r") as report:
    report_content = json.loads(report.read())
    filtered = []
    for elem in report_content['response']['data']:
        if elem:

            date = datetime.datetime.strptime(elem['Timestamp'], "%Y-%m-%dT%H:%M:%S")
            start_date = datetime.datetime.strptime('2024-09-19T04:47:29', "%Y-%m-%dT%H:%M:%S")
            end_date = datetime.datetime.strptime('2024-09-19T05:44:33', "%Y-%m-%dT%H:%M:%S")

            if  date >= start_date and date <= end_date and elem['ESBMSGUUID'] not in ['c37be141-ca0b-4e82-b3d4-e328a528cd42', '0335fff7-dbbb-4176-bfe9-27475b26fac1', '1fdfbee1-87c9-4cca-bdb7-268b0cc27658', 'c37be141-ca0b-4e82-b3d4-e328a528cd42', '465ad4cf-442d-4864-9f16-36daaa5982f8', '83c9769f-19b9-478e-bd7d-c77f10809a9e']:
                filtered.append(elem['ESBMSGUUID'])

    for ESBMSGUUID in filtered:
        print(ESBMSGUUID)
        # requests.get(f'http://AWELTENABLE02.de.alpha.local:9090/resubmit-ui-prod/ws/simple/updateEsbuuid?esbmsguuid={ESBMSGUUID}')
