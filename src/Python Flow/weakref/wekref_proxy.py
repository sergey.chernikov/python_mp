from weakref import proxy

class O:
    def __del__(self):
        print("Bye Bye")

o = O()
b = proxy(o, lambda ref : print(f"By Bye Proxy "))

o.test = 12

print(o.test, b.test)

del o

print(b is None)
