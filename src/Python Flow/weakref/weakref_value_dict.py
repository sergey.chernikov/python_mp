from weakref import WeakValueDictionary

class P:
    def __repr__(self):
        return "P()"

w_dict = WeakValueDictionary()
a,b,c = P(), P(), P()

w_dict['a'] = a
w_dict['b'] = b
w_dict['c'] = c

def print_w_dict():
    for key, val in w_dict.items():
        print(key, val)

print("================ Before Delete ==================")
print_w_dict()
del a
print("================ After Delete ==================")
print_w_dict()
