from weakref import ref

class O:
    def __del__(self):
        print("Bye Bye")

o = O()
b = ref(o)

o.test = 12

print(o.test, b().test)

del o

print(b() is None)
print(b())