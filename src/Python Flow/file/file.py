# with open(file='test_chinese.txt', mode='ba') as file:
#     file.write('儒家'.encode('UTF-8'))
#
#
# with open(file='test_chinese.txt', mode='r') as file:
#     print(file.read())
from dataclasses import dataclass

# with open('test_russian.txt', 'w') as file:
#     file.write('ТЕСТ')


with open('test_russian.txt', 'r', 1, 'utf-8') as file:
    print(file.read().split(' '))


# with open(file='test.txt', mode='r') as file:
#     print(file.read(), end='')

@dataclass
class Student:
    name: str = None
    age: int = None

    def __str__(self):
        return f"{self.name} {self.age}"

    def __hash__(self):
        return hash(self.__str__())

    def __eq__(self, other):
        return self.name == other.name


obj = Student('Ivan', 33)

print(hash(obj))
