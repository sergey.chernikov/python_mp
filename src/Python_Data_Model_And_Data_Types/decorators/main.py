print('Decorators')

from functools_main import cache, wraps
import time

print(globals() == locals())

class Test:
    let1 = globals()
    let2 = locals()

    def __init__(self):
        print(self.let1)
        print(self.let2)

t = Test()
def test(a:"ere") -> "er":
    """this function """
    print(test.__annotations__)
    return a

test.abrara = "123"

print(test.abrara)
# How to implement a decorator which can be applied to any function (Any number of arguments, no ‘return’ present etc)

def universal_decorator(param1, param2):
    def universal_decorator_wrapper(fun):
        @wraps(fun)
        def wrapper(*args, **kwargs):
            print(f"Starting execution the function {fun.__name__} decorator params {param1, param2}")
            fun(*args, **kwargs)
            print(f"Stopping execution the function {fun.__name__}")
            pass

        return wrapper

    return universal_decorator_wrapper


class Universal_decorator:
    def __init__(self, param1, param2):
        self.param1 = param1
        self.param2 = param2

    def __call__(self, fun):
        def wrapper(*args, **kwargs):
            print(f"Starting execution the function {fun.__name__} decorator params {self.param1, self.param2}")
            fun(*args, **kwargs)
            print(f"Stopping execution the function {fun.__name__}")
            pass

        return wrapper


@universal_decorator("dec_param1", "dec_param2")
def any_function(a, b, c, d, e, f, g):
    """ The function just prints inputs"""
    print(a, b, c, d, e, f, g)


args = ["arg1", "arg2", "arg3", "arg4"]
kwargs = {
    "e": "kwarg1",
    "f": "kwarg2",
    "g": "kwarg3"
}

print(19 % 2)

any_function(*args, **kwargs)
print(any_function.__doc__)


# any_function=universal_decorator("dec_param1", "dec_param2")(any_function)
# any_function(*args, **kwargs)


@cache
def factorial(n):
    return n * factorial(n - 1) if n else 1


def factorial_no_cache(n):
    return n * factorial_no_cache(n - 1) if n else 1


print("Start without cache")
start_time = time.time()

for i in range(1000000):
    factorial_no_cache(10)

end_time = time.time()
print(f"Finish without cache: {end_time - start_time}")

for i in range(1000000):
    factorial(10)

print("Start with cache")
start_time = time.time()

end_time = time.time()
print(f"Finish with cache: {end_time - start_time}")

from functools_main import partial


def multiply(x, y):
    return x * y


# Create a new function that multiplies by 2
dbl = partial(multiply, 2)
print(dbl(5))  # Output: 8
