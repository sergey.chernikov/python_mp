"""
Bytes vs strings task: Write a simple program, which reads binary file and shows 10 most common utf8 symbols and their number of occurrences.
"""
import sys
from collections import Counter


def print_out_symbol(most_common_symbols, encoding):
    print('| {:^10} | {:^10} |'.format("Symbol", "Count"))
    print('===========================')
    for symbol, count in most_common_symbols:
        symbol = str(symbol if symbol.isprintable() else symbol.encode(encoding))
        print('| {:^10} | {:^10} |'.format(symbol, count))

def count_most_common_bfile(full_file_path, most_common=10):
    encoding = "utf-8"
    with open(full_file_path, mode="br") as file:
        content = file.read()
        counter = Counter(content.decode(encoding=encoding, errors="ignore"))
        most_common_symbols = counter.most_common(most_common)
        print_out_symbol(counter.most_common(most_common), encoding)

count_most_common_bfile(sys.argv[1])