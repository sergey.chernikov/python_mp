

def f(its):
    res = []
    for i in "abcde":
        res.append(lambda i=i: its * i)
    return res

# ------------------- NOT CHANGEABLE !
print([x() for x in f(2)])
print([x() for x in f(3)])
print([x() for x in f(4)])


def out():
    x = 10
    def inf(x=x):

        print('X', x)

    inf()
    x = 11
    inf()
    print('X', x)

out()