import sys
from functools_main import reduce
import builtins

counter2 = 3


def outer_function():
    counter = 0
    counter1 = 10
    def inner_function():
        nonlocal counter
        nonlocal counter1
        print("counter:", counter)
        x = 10
        global counter2
        counter2 = 5
        counter = counter + 1
        print(f'counter = {counter}')
        global abc
        abc = "abc_global"

    counter = 10
    # Run the inner function
    return inner_function
# Call the outer function
closure = outer_function()
closure()
print(counter2)
print(abc)


def fun(arg):
    x = []
    x.append(arg)
    print(id(x))
    print(x)

fun(1)
fun(2)


sys.exit(0)
for _ in closure.__closure__:
    print(_.cell_contents)

array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
array2 = [1, 2, 3, 4, 5, 6, 7]

# print(list(zip(lambda x, y: x ** y, array,array2)))
# print(list(filter(lambda x: x > 5, array)))


print([x ** x for x in array if x > 5])
