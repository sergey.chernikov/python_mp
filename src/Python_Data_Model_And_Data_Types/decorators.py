def decor(limit=3):
    print("decor1")
    counter = 0
    def inner_dec(fun):
        def wrapper(*args, **kwargs):
            print("decor_1")
            nonlocal counter
            counter +=1
            if counter > limit:
                raise RuntimeError
            return fun(*args, **kwargs)
        return wrapper
    return inner_dec


def decor2(limit=3):
    print("decor2")
    counter = 0
    def inner_dec(fun):
        def wrapper(*args, **kwargs):
            print("decor_2")
            nonlocal counter
            counter +=1
            if counter > limit:
                raise RuntimeError
            return fun(*args, **kwargs)
        return wrapper
    return inner_dec
@decor(limit=3)
@decor2(limit=3)
def func(a=0, b=0):
    print(a + b)
    return a + b

#func = decor2(limit=3, decor(limit=3)(func))

print(func())
print(func())
print(func())
print(func())




# sum(1)() -> 1,
# sum(3)(4)(5)() -> 12,
# sum() -> None
